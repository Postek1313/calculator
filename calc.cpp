#include "calc.h"
#include "ui_calc.h"

double calcVal=0.0;
double memoryVal = 0.0;
QString memoryDebug ="";

bool divTrigger=false;
bool addTrigger=false;
bool mulTrigger=false;
bool subTrigger=false;

calc::calc(QWidget *parent): QMainWindow(parent), ui(new Ui::calc)
{
    ui->setupUi(this);
    ui->Display->setText(QString::number(calcVal));
    QPushButton *numButtons[10];

    for(int i=0;i<10;i++) {
    QString butName="Button"+QString::number(i);
            numButtons[i]=  calc::findChild<QPushButton*>(butName);
            connect(numButtons[i],SIGNAL(released()),this,SLOT(NumPress()));
    }

    connect(ui->ButtonClear,SIGNAL(released()),this,SLOT(Clear()));
    connect(ui->ButtonChangeSign,SIGNAL(released()),this,SLOT(ChangeNumSign()));
    connect(ui->ButtonEqual,SIGNAL(released()),this,SLOT(EqualButton()));

    connect(ui->ButtonAdd,SIGNAL(released()),this,SLOT(MathButtonPress()));
    connect(ui->ButtonDivide,SIGNAL(released()),this,SLOT(MathButtonPress()));
    connect(ui->Buttonmultiple,SIGNAL(released()),this,SLOT(MathButtonPress()));
    connect(ui->ButtonSubtract,SIGNAL(released()),this,SLOT(MathButtonPress()));

    connect(ui->ButtonMplus,SIGNAL(released()),this,SLOT(AddToMemory()));
    connect(ui->ButtonMminus,SIGNAL(released()),this,SLOT(SubFromMemory()));
    connect(ui->ButtonMemory,SIGNAL(released()),this,SLOT(ShowMemoryValue()));




}

calc::~calc()
{
    delete ui;
}

void calc::NumPress()
{
QPushButton *button=(QPushButton*)sender(); // sender zwraca wskaźnik do wywołanego przycisku
QString butValue=button->text();            // wartosc przycisku 0-9
QString displayValue=ui->Display->text();
if (displayValue.toDouble()==0 || displayValue.toDouble()==0.0) {
        ui->Display->setText(butValue);
}
else{
    QString newValue = displayValue + butValue;
    ui->Display->setText(newValue);
}

}

void calc::MathButtonPress()
{
    divTrigger=false;
    addTrigger=false;
    mulTrigger=false;
    subTrigger=false;
    if (ui->Display->text()!="") {
        QString displayValue=ui->Display->text();
        calcVal=displayValue.toDouble();

        QPushButton *button=(QPushButton*)sender();
        QString butValue=button->text();
        if(QString::compare(butValue,"/",Qt::CaseSensitive)==0) {
        divTrigger=true;
        }
        else if(QString::compare(butValue,"*",Qt::CaseSensitive)==0){
        mulTrigger=true;
        }
        else if(QString::compare(butValue,"+",Qt::CaseSensitive)==0){
        addTrigger=true;
        }
        else if(QString::compare(butValue,"-",Qt::CaseSensitive)==0){
        subTrigger=true;
        }


        ui->Display->setText("");
    }
    else{
        divTrigger=false;
        addTrigger=false;
        mulTrigger=false;
        subTrigger=false;
         ui->Display->setText(QString::number(calcVal));
    }
}

void calc::EqualButton()
{
    double solution=0;
    QString displayValue=ui->Display->text();
    double doubleDisplayValue=displayValue.toDouble();
    if(divTrigger || mulTrigger || addTrigger || subTrigger) {
        if (mulTrigger) {solution=calcVal*doubleDisplayValue; }
        else if (subTrigger) {solution=calcVal-doubleDisplayValue; }
        else if (divTrigger) {solution=calcVal/doubleDisplayValue; }
        else if (addTrigger) {solution=calcVal+doubleDisplayValue; }
    }
    ui->Display->setText(QString::number(solution));
}

void calc::ChangeNumSign()
{
    QString displayValue=ui->Display->text();
    double dvalue=displayValue.toDouble();
    double solution=dvalue*(-1);

    ui->Display->setText(QString::number(solution));


}

void calc::Clear()
{
ui->Display->setText("0");
calcVal=0;

}

void calc::AddToMemory()
{
     QString displayValue=ui->Display->text();
     memoryVal=memoryVal+displayValue.toDouble();
}

void calc::SubFromMemory()
{
    QString displayValue=ui->Display->text();
    memoryVal=memoryVal-displayValue.toDouble();
}

void calc::ShowMemoryValue()
{
    ui->Display->setText(QString::number(memoryVal));
}



